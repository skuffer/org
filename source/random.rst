.. random:

Random Links
============

Some Links to Videos, Blogs, New's Sites, ...


- Ben Eater's `8-bit breadboard computer <https://www.youtube.com/playlist?list=PLowKtXNTBypGqImE405J2565dvjafglHU>`_
  playlist.
- `pycoders <https://pycoders.com/>`_ Python weekly newsletter
- `Feisty Duck Newsletter <https://www.feistyduck.com/bulletproof-tls-newsletter/>`_
  on Secure SSL/TLS
- `ycombinator news <https://news.ycombinator.com/>`_
