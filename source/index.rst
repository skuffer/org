.. skuffer.org documentation master file, created by
   sphinx-quickstart on Wed Jun  3 08:11:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to skuffer.org
======================

.. toctree::
   :caption: Pages
   :maxdepth: 1

   about
   blog
   random



.. toctree::
    :caption: Blog entries
    :titlesonly:
    :glob:
    :reversed:

    blog/*