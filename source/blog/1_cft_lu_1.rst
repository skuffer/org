.. cft_lu_1:

A CTF in Luxembourg - Looking for a Ghost
=========================================

    Published on 05.06.2020

A couple month's back, while browsing reddit, I stumbled onto a `CTF held in
Luxembourg <https://ctf.cybersecuritychallenge.lu>`_.
Having never participated in a CTF, I though I give it a try.

.. note::
    The write up below contains the answer to one of the challenges, don't read
    it if you wan't to give it a go.

The event was supposed to happen on location - but COVID - and so they moved to
an online only format, which I forgot about and ended up never attending. Ups,
shortest write up ever... Never the less, they provide some challenges online,
so I could still try some out.

At the time of writing, I only solved two challenges, but have tried many.
That's part of the learning process I guess. As my day job consists of creating
Web-Applications, I wasn't so eager to try the only Web challenge, but was drawn
to the Steganography category. More specifically ``Ghost In The Wire``.

The challenge
-------------

    Yesterday someone had the audacity to call me in the middle of the night! It
    was just a few seconds long and sounded strange, maybe it was a ghost?! I
    can't but wonder what it was trying to tell me. Can you help me figuring it
    out? Maybe we need to look at it trough different glasses.

They provided you with a  `.wav` file.

Ok, so it's a audio file, first things first; let's listen to it... It's 5
seconds long, contains a lot of high pitch noise with short breaks in between.
Maybe, reducing the playback speed could help - Nothing.
VLC has a option vo visualize a audio stream:

.. figure:: ../_static/ghost_in_wire_random.png

Looks random, but it shouldn should it? For comparison, here how a normal
song would look with the same visualization:

.. figure:: ../_static/ghost_in_wire_song.png

and sometimes you would get a perfect waves in the challenge file:

.. figure:: ../_static/ghost_in_wire_wave.png

So something is definitely special with the file. Then I remembered a video a
work colleague once showed about the `creation of the Doom soundtracks by Mick
Gordon <https://www.youtube.com/watch?v=Pu4dB_Wy1-E&t=2210>`_ and that you can
hide text and images in a song. And if you can hide data in a actual song, you
can definitely hide something in a sound file that isn't close to any actual
music. A quick search later I've downloaded and installed the Acoustic Spectrum
Analyser `Spek <http://spek.cc>`_ and:

.. figure:: ../_static/ghost_in_wire_spek.png

But wait, there is more! The thing to note is that the string ends with a double
equal sign, and that sort of thing screams base64.

::

    > echo "dGhlcmVfYXJlX25vX2dob3N0cw==" | base64 -d
    there_are_no_ghosts

And there is the flag.