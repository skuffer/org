.. blog:


Blog
====

.. toctree::
    :caption: Blog entries
    :titlesonly:
    :glob:
    :reversed:

    blog/*
