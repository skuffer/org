.. about:

About Me
========

Hi, I'm Sam. During the day I'm a Python/Django Developer, by night I do lot's
of things: Triathlon (just starting), climbing (it's been some time),
3D Printing, and getting my feet wet with a
`CTF <https://ctf.cybersecuritychallenge.lu/challenges>`_ and `Over the wire
<https://overthewire.org/wargames/>`_.

The motivation to create this site came from `Tiny Projects
<https://tinyprojects.dev/>`_.
Not all of it, I don't care for handwritten HTML/CSS/JS, but I **really**
appreciate a fast website in this day and age without any cookie accepting
pop-up or or to download a couple of mega bytes only to view index, and all the
other horrible stuff that we sadly accepted as the new Web. You know, simpler
times.

I'm not one for *Social Networks*, so if you want to contact me, the best
way is via `email <mailto:sam@skuffer.org>`_
